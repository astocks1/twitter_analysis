# Twitter sentiment analysis

Work for course "Sistemas de Información y Tecnología". Master in Management and Analytics.

Uses twitter's API to search for specific terms and analyze them.  Uses Elastic Search for storing and searching terms via inverse indexing.

Requires twitter API keys to run.
