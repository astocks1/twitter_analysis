import json
import os
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import sys
import tweepy
from tweepy import Stream

# Output directory
output_directory = "tweets"

# This is a basic listener that batches tweets and stores them in Parquet.
class TweetListener(tweepy.streaming.StreamListener):

    def __init__(self, batch_size = 10000, brand=None):
        super().__init__()
        self.tweets_buffer = []
        self.iter = 0
        self.batch_size = batch_size
        self.df = pd.DataFrame(columns=["created_at","username","tweet","final_coordinates","user_tz",
                               "user_location","retweeted"])
    def on_data(self, data):
        all_data = json.loads(data)
		# collect all desired data fields
        if not 'created_at' in all_data:
            return True
        if 'text' in all_data:
            if 'extended_tweet' in all_data:
                tweet = all_data['extended_tweet']['full_text']
            else: tweet = all_data ['text']
            created_at    = all_data["created_at"]
            retweeted     = all_data["retweeted"]
            username      = all_data["user"]["screen_name"]
            user_tz       = all_data["user"]["time_zone"]
            user_location = all_data["user"]["location"]
            user_coordinates   = all_data["coordinates"]
	  # if coordinates are not present store blank value
	  # otherwise get the coordinates.coordinates value
            if user_coordinates is None:
                final_coordinates = user_coordinates
            else:
                final_coordinates = str(all_data["coordinates"]["coordinates"])

        print ('Iter:', str(self.iter), '.', str(len (self.tweets_buffer)), created_at, tweet)

    #Logica para generar distintos archivos parquet
        if len (self.tweets_buffer) < self.batch_size:
            self.df = self.df.append({"created_at": created_at, "username": username, "tweet": tweet, "final_coordinates": final_coordinates,
                             "user_tz": user_tz, "user_location": user_location, "retweeted": retweeted}, ignore_index=True)
            self.tweets_buffer.append(tweet)

        else:
            table = pa.Table.from_pandas(self.df)
            file_name = 'tweets_xbox' + str(self.iter) + '.parquet'
            pq.write_table(table, file_name)
            self.iter += 1
            self.tweets_buffer.clear()
            self.df = pd.DataFrame(columns=["created_at","username","tweet","final_coordinates","user_tz",
                               "user_location","retweeted"])


    def on_error(self, status):
        print('Something went wrong. Status', status)

if __name__ == '__main__':
    # Parse arguments that contains the user credentials to access Twitter API
    if len(sys.argv) < 5 or len(sys.argv) > 6:
        print('Usage: <access_token> <access_token_secret> <consumer_key> <consumer_secret> [brand]')
        sys.exit(1)
    access_token = sys.argv[1]
    access_token_secret = sys.argv[2]
    consumer_key = sys.argv[3]
    consumer_secret = sys.argv[4]
    if len(sys.argv) == 6:
       brand = sys.argv[5]
    else:
       brand = None

    # Create output directory if it does not yet exist
    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    # This handles Twitter authentication and the connection to Twitter Streaming API
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    ##### Run Tweetlistener to get stream of tweets
    l = TweetListener()
    twitterStream = Stream(auth, l)
    print(brand)
    if brand is None:
        twitterStream.sample()
    else:
        twitterStream.filter(track=[brand],languages = ["en"], stall_warnings = True)
