#!/usr/bin/env python

import os
import sys
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from collections import defaultdict

def sanitize(word):
   # normalize
    return word

if __name__ == '__main__':
    words = {}
#    nltk.download() #Se utilizan los stopwords de la libreria NLTK, esto se debe descargar por lo menos una vez.
    stop = stopwords.words('english')
    output = sys.argv[2]
    outputfile = open(output, 'w')
    brand = 'control'
    st = PorterStemmer() #Definicion de objeto para stemming
    if len(sys.argv) == 4:
        brand = sys.argv[3]
    for inputfile in os.listdir(sys.argv[1]):
        filename = str(sys.argv[1]) + '\\' + inputfile
        Table = pq.read_table(filename)
        df = Table.to_pandas()

        df['tweet'] = df['tweet'].apply(lambda x: " ".join(x.lower() for x in x.split())) #Utilizamos libreria NLTK para convertir todos los tweets del parquet a minuscula
        df['tweet'] = df['tweet'].apply(lambda x: " ".join(x for x in x.split() if x not in stop))  #Utilizamos libreria NLTK para limpiar stopwords sobre todo el archivo parquet
        df['tweet'] = df['tweet'].str.replace('[^\w\s]','')  #Sacamos puntuacion
        df['tweet'] = df['tweet'].apply(lambda x: " ".join([st.stem(word) for word in x.split()]))

        for index, row in df.iterrows():
            tweet = str.split(row['tweet'])
            for word in tweet:
                if len(word) == len(word.encode()):
                    #words[sanitize(word)] = 1
                    outputfile.write(sanitize(word) + "\t" + brand + "\t" + str(1) + "\n")
        pass
    print(words)

    #ASEGURARSE DE TENER UN ARCHIVO .TSV CREADO PARA PASAR COMO 2DO ARGUMENTO
#    for key, count in words.items():
#        print (key, count)
#        val = key + "\t" + brand + "\t" + str(count) + "\n"
#        val = val.encode("utf-8")
#        outputfile.write(key + "\t" + brand + "\t" + str(count) + "\n")
    outputfile.close()
