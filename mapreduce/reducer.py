#!/usr/bin/env python

import sys
from elasticsearch import Elasticsearch

elastic = Elasticsearch([{'host': 'localhost', 'port': 9200}])

last_key = None
running_totals = {}

def process_term(term):
   freq_xbox=0
   freq_ps=0
   freq_control=0

   print('Term:', term)
   print('Running totals:', running_totals)
   # creamos diccionario temporario con término y totales de cada termino para indexar en ES
   if 'ps' in running_totals.keys():
       freq_ps = running_totals['ps']

   if 'xbox' in running_totals.keys():
       freq_xbox = running_totals['xbox']

   if 'control' in running_totals.keys():
       freq_control = running_totals['control']

   document = {
   'term' : term,
   'frequency_ps': freq_ps,
   'freq_xbox': freq_xbox,
   'frequency_control': freq_control,
   'freq_total': freq_ps + freq_xbox + freq_control
   }

   index='tweets' #definimos en qué índice queremos guardar el término
   elastic.index(index=index,doc_type='word', id=last_key, body = document, op_type='index')
   pass

def collect(brand, count):
   if brand in running_totals:
      running_totals[brand] += count
   else:
      running_totals[brand] = count


if __name__ == '__main__':
   inputfile = open(sys.argv[1], 'r')
   for input_line in inputfile:
      input_line = input_line.strip()
      this_key, brand, count = input_line.split("\t", 2)
      count = int(count)
      if last_key == this_key:
         collect(brand, count)
      else:
         if last_key:
            process_term(last_key)
         running_totals = {}
         collect(brand, count)
         last_key = this_key
   if last_key == this_key:
      process_term(last_key)

   test = elastic.search(index="tweets", body={"query": {"match": {'term':'gaming'}}})
   print('Test', test)
   #es.search(index="tweets")
